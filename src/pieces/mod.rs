use crate::board;
pub mod moves;

#[derive(Clone, Copy)]
pub enum ChessPiece {
    WhiteKing,
    WhiteQueen,
    WhiteRook,
    WhiteKnight,
    WhiteBishop,
    WhitePawn,
    BlackKing,
    BlackQueen,
    BlackRook,
    BlackKnight,
    BlackBishop,
    BlackPawn,
    Empty,
}

impl PartialEq for ChessPiece {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (ChessPiece::WhiteKing, ChessPiece::WhiteKing)
            | (ChessPiece::WhiteQueen, ChessPiece::WhiteQueen)
            | (ChessPiece::WhiteRook, ChessPiece::WhiteRook)
            | (ChessPiece::WhiteKnight, ChessPiece::WhiteKnight)
            | (ChessPiece::WhiteBishop, ChessPiece::WhiteBishop)
            | (ChessPiece::WhitePawn, ChessPiece::WhitePawn)
            | (ChessPiece::BlackKing, ChessPiece::BlackKing)
            | (ChessPiece::BlackQueen, ChessPiece::BlackQueen)
            | (ChessPiece::BlackRook, ChessPiece::BlackRook)
            | (ChessPiece::BlackKnight, ChessPiece::BlackKnight)
            | (ChessPiece::BlackBishop, ChessPiece::BlackBishop)
            | (ChessPiece::BlackPawn, ChessPiece::BlackPawn)
            | (ChessPiece::Empty, ChessPiece::Empty) => true,
            _ => false,
        }
    }
}

pub fn get_html_entity(piece: &ChessPiece) -> &'static str {
    match piece {
        ChessPiece::WhiteKing => "♔",
        ChessPiece::WhiteQueen => "♕",
        ChessPiece::WhiteRook => "♖",
        ChessPiece::WhiteBishop => "♗",
        ChessPiece::WhiteKnight => "♘",
        ChessPiece::WhitePawn => "♙",
        ChessPiece::BlackKing => "♚",
        ChessPiece::BlackQueen => "♛",
        ChessPiece::BlackRook => "♜",
        ChessPiece::BlackBishop => "♝",
        ChessPiece::BlackKnight => "♞",
        ChessPiece::BlackPawn => "♟︎",
        ChessPiece::Empty => "",
    }
}


pub fn get_possible_moves<'a>(
    board: &'a Vec<Vec<board::BoardSquare>>,
    field: &str
) -> Vec<&'a str> {
    let mut possible_moves: Vec<&str> = vec![];
    for row in 0..board::COLUMNS {
        for column in 0..board::ROWS {
            let current_square = &board[row][column];

            if current_square.field == field {
                let piece = &current_square.current_value;
                let is_black = match piece {
                    ChessPiece::BlackPawn
                    | ChessPiece::BlackKnight
                    | ChessPiece::BlackRook
                    | ChessPiece::BlackBishop
                    | ChessPiece::BlackQueen
                    | ChessPiece::BlackKing => true,
                    _ => false,
                };

                possible_moves = match piece {
                    ChessPiece::BlackPawn | ChessPiece::WhitePawn => moves::get_moves_for_pawn(board, row, column, is_black),
                    ChessPiece::BlackKnight | ChessPiece::WhiteKnight => moves::get_moves_for_knight(board, row, column, is_black),
                    ChessPiece::BlackRook | ChessPiece::WhiteRook => moves::get_moves_for_rook(board, row, column, is_black),
                    ChessPiece::BlackBishop | ChessPiece::WhiteBishop => moves::get_moves_for_bishop(board, row, column, is_black),
                    ChessPiece::BlackQueen | ChessPiece::WhiteQueen => moves::get_moves_for_queen(board, row, column, is_black),
                    ChessPiece::BlackKing | ChessPiece::WhiteKing => moves::get_moves_for_king(board, row, column, is_black),
                    _ => Vec::new(),
                };
            }
        }
    }
    return possible_moves;
}

