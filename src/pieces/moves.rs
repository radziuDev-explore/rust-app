use crate::board;
use crate::pieces::ChessPiece;

pub fn get_moves_for_pawn(
    board: &Vec<Vec<board::BoardSquare>>,
    row: usize,
    column: usize,
    is_black: bool,
) -> Vec<&str> {
    let mut possible_moves: Vec<&str> = Vec::new();

    let direction: i32 = if is_black { 1 } else { -1 };
    let starting_row = if is_black { 1 } else { 6 };

    if (is_black && row >= 7) || (!is_black && row <= 0) {
        return possible_moves;
    }

    if row as i32 + direction < 0 {
        return possible_moves;
    }

    let cell = &board[(row as i32 + direction) as usize][column];
    if cell.current_value == ChessPiece::Empty {
        possible_moves.push(&cell.field);
    }

    if row == starting_row {
        let cell2 = &board[(row as isize + 2 * direction as isize) as usize][column];
        if cell2.current_value == ChessPiece::Empty {
            possible_moves.push(&cell2.field);
        }
    }

    for &col_offset in [-1, 1].iter() {
        let new_column = (column as isize + col_offset) as usize;
        if new_column < 8 {
            let enemy_cell = &board[(row as isize + direction as isize) as usize][new_column];
            if board::utils::is_white_piece(board, (row as i32 + direction) as usize, new_column) {
                possible_moves.push(&enemy_cell.field);
            }
        }
    }

    return possible_moves;
}

pub fn get_moves_for_knight(
    board: &Vec<Vec<board::BoardSquare>>,
    row: usize,
    column: usize,
    is_black: bool,
) -> Vec<&str> {
    let mut possible_moves: Vec<&str> = Vec::new();

    let knight_moves = [
        (-2, -1),
        (-2, 1),
        (-1, -2),
        (-1, 2),
        (1, -2),
        (1, 2),
        (2, -1),
        (2, 1),
    ];

    for &(r, c) in &knight_moves {
        if row as i32 + r >= 0
            && row as i32 + r <= 7
            && column as i32 + c >= 0
            && column as i32 + c <= 7
        {
            let row_offset = (row as i32) + r;
            let column_offset = (column as i32) + c;

            get_avalible_moves(
                board,
                row_offset,
                column_offset,
                &mut possible_moves,
                is_black,
            );
        }
    }

    return possible_moves;
}

pub fn get_moves_for_rook(
    board: &Vec<Vec<board::BoardSquare>>,
    row: usize,
    column: usize,
    is_black: bool,
) -> Vec<&str> {
    let mut possible_moves: Vec<&str> = Vec::new();
    let rook_moves: Vec<(i32, i32, i32, i32, i32, i32)> = vec![
        (1, -1, 1, 0, 1, row as i32),
        (0, 1, 1, 0, row as i32 + 1, board::ROWS as i32 - 1),
        (1, 0, 1, -1, 1, column as i32),
        (1, 0, 0, 1, column as i32 + 1, board::COLUMNS as i32 - 1),
    ];

    for &(r, rn, c, cn, from, to) in &rook_moves {
        for num in from..=to {
            let row_offset = (row as i32 * r) + (num as i32 * rn);
            let column_offset = (column as i32 * c) + (num as i32 * cn);

            if get_avalible_moves(
                board,
                row_offset,
                column_offset,
                &mut possible_moves,
                is_black,
            ) {
                break;
            }
        }
    }

    return possible_moves;
}

pub fn get_moves_for_bishop(
    board: &Vec<Vec<board::BoardSquare>>,
    row: usize,
    column: usize,
    is_black: bool,
) -> Vec<&str> {
    let mut possible_moves: Vec<&str> = Vec::new();
    let bishop_moves: Vec<(i32, i32)> = vec![(-1, -1), (-1, 1), (1, -1), (1, 1)];
    for &(x, y) in &bishop_moves {
        for num in 1..=8 {
            let row_offset = (row as i32) + ((num as i32) * x);
            let column_offset = (column as i32) + ((num as i32) * y);

            if get_avalible_moves(
                board,
                row_offset,
                column_offset,
                &mut possible_moves,
                is_black,
            ) {
                break;
            }
        }
    }

    return possible_moves;
}

pub fn get_moves_for_queen(
    board: &Vec<Vec<board::BoardSquare>>,
    row: usize,
    column: usize,
    is_black: bool,
) -> Vec<&str> {
    let mut possible_moves: Vec<&str> = Vec::new();

    let rook_moves = get_moves_for_rook(board, row, column, is_black);
    let bishop_moves = get_moves_for_bishop(board, row, column, is_black);

    possible_moves.extend(rook_moves.iter().cloned());
    possible_moves.extend(bishop_moves.iter().cloned());

    return possible_moves;
}

pub fn get_moves_for_king(
    board: &Vec<Vec<board::BoardSquare>>,
    row: usize,
    column: usize,
    is_black: bool,
) -> Vec<&str> {
    let mut possible_moves: Vec<&str> = Vec::new();
    let king_moves: Vec<(i32, i32)> = vec![
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];

    for &(x, y) in &king_moves {
        let row_offset = (row as i32) + (x as i32);
        let column_offset = (column as i32) + (y as i32);

        get_avalible_moves(
            board,
            row_offset,
            column_offset,
            &mut possible_moves,
            is_black,
        );
    }

    return possible_moves;
}

fn get_avalible_moves<'a>(
    board: &'a Vec<Vec<board::BoardSquare>>,
    row_offset: i32,
    column_offset: i32,
    possible_moves: &mut Vec<&'a str>,
    is_black: bool,
) -> bool {
    if row_offset < 0 || column_offset < 0 {
        return true;
    }

    let x = row_offset as usize;
    let y = column_offset as usize;

    if board::utils::is_empty(board, x, y) {
        possible_moves.push(&board[x][y].field);
    }

    if is_black {
        if board::utils::is_white_piece(board, x, y) {
            possible_moves.push(&board[x][y].field);
            return true;
        }

        if board::utils::is_black_piece(board, x, y) {
            return true;
        }
    } else {
        if board::utils::is_black_piece(board, x, y) {
            possible_moves.push(&board[x][y].field);
            return true;
        }

        if board::utils::is_white_piece(board, x, y) {
            return true;
        }
    }
    return false;
}
