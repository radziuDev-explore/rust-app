use crate::board;
use crate::pieces;

pub struct Game {
    pub turn: i32,
    pub is_black_turn: bool,
    pub board: Vec<Vec<board::BoardSquare>>,
}

pub fn create_game() -> Game {
    let mut board = board::create_board();
    board::fill_board(&mut board);
    board::fill_start_position(&mut board);
    board::draw_board(&board);

    return Game {
        turn: 0,
        is_black_turn: false,
        board,
    };
}

pub fn make_turn(game: &mut Game, from: &str, to: &str) {
    let mut from_row: usize = 0;
    let mut to_row: usize = 0;
    let mut from_column: usize = 0;
    let mut to_column: usize = 0;

    for r in 0..=board::ROWS - 1 {
        for c in 0..=board::COLUMNS - 1 {
            if from == game.board[r][c].field {
                from_row = r;
                from_column = c;
            }
            if to == game.board[r][c].field {
                to_row = r;
                to_column = c;
            }
        }
    }

    if game.is_black_turn && !board::utils::is_black_piece(&game.board, from_row, from_column) {
        print!("You need choose black piece! \n");
        return;
    }
    if !game.is_black_turn && !board::utils::is_white_piece(&game.board, from_row, from_column) {
        print!("You need choose white piece! \n");
        return;
    }

    let possible_moves = pieces::get_possible_moves(&game.board, from);
    if !possible_moves.iter().any(|&x| x == to) {
        print!("Illegal move! \n");
        return;
    }

    let piece = game.board[from_row][from_column].current_value;
    game.board[from_row][from_column].current_value = pieces::ChessPiece::Empty;
    game.board[to_row][to_column].current_value = piece;
    game.is_black_turn = !game.is_black_turn;
    game.turn = game.turn +1;

    print!("      Turn: {} \n", game.turn);
    board::draw_board(&game.board);
}
