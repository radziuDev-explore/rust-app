mod board;
mod pieces;
mod game;

fn main() {
    let mut game = game::create_game();
    game::make_turn(&mut game, "A2", "A4");
    game::make_turn(&mut game, "E7", "E5");
    game::make_turn(&mut game, "B1", "C3");
    game::make_turn(&mut game, "E8", "E7");
}
