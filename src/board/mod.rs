use crate::pieces;
pub mod utils;

pub const ROWS: usize = 8;
pub const COLUMNS: usize = 8;
pub const WHITE_FIELD: &str = "□";
pub const BLACK_FIELD: &str = "■";

pub struct BoardSquare {
    pub field: String,
    pub default_field: String,
    pub current_value: pieces::ChessPiece,
}

pub fn create_board() -> Vec<Vec<BoardSquare>> {
    let mut board: Vec<Vec<BoardSquare>> = Vec::new();

    for _ in 0..ROWS {
        let mut row = Vec::new();
        for _ in 0..COLUMNS {
            row.push(BoardSquare {
                field: String::new(),
                default_field: String::new(),
                current_value: pieces::ChessPiece::Empty,
            });
        }
        board.push(row);
    }

    return board;
}

pub fn fill_board(board: &mut Vec<Vec<BoardSquare>>) {
    let letters = ["A", "B", "C", "D", "E", "F", "G", "H"];

    for number in 0..ROWS {
        for letter in 0..COLUMNS {
            let square = &mut board[number][letter];
            square.field = format!("{}{}", letters[letter], COLUMNS - number);
            square.current_value = pieces::ChessPiece::Empty;
            square.default_field = if (number + letter) % 2 == 0 {
                WHITE_FIELD.to_string()
            } else {
                BLACK_FIELD.to_string()
            };
        }
    }
}

pub fn fill_start_position(board: &mut Vec<Vec<BoardSquare>>) {
    for number in 0..ROWS {
        for letter in 0..COLUMNS {
            let square = &mut board[number][letter];
            match square.field.as_str() {
                "A8" => square.current_value = pieces::ChessPiece::BlackRook,
                "B8" => square.current_value = pieces::ChessPiece::BlackKnight,
                "C8" => square.current_value = pieces::ChessPiece::BlackBishop,
                "D8" => square.current_value = pieces::ChessPiece::BlackQueen,
                "E8" => square.current_value = pieces::ChessPiece::BlackKing,
                "F8" => square.current_value = pieces::ChessPiece::BlackBishop,
                "G8" => square.current_value = pieces::ChessPiece::BlackKnight,
                "H8" => square.current_value = pieces::ChessPiece::BlackRook,

                "A7" => square.current_value = pieces::ChessPiece::BlackPawn,
                "B7" => square.current_value = pieces::ChessPiece::BlackPawn,
                "C7" => square.current_value = pieces::ChessPiece::BlackPawn,
                "D7" => square.current_value = pieces::ChessPiece::BlackPawn,
                "E7" => square.current_value = pieces::ChessPiece::BlackPawn,
                "F7" => square.current_value = pieces::ChessPiece::BlackPawn,
                "G7" => square.current_value = pieces::ChessPiece::BlackPawn,
                "H7" => square.current_value = pieces::ChessPiece::BlackPawn,

                "A2" => square.current_value = pieces::ChessPiece::WhitePawn,
                "B2" => square.current_value = pieces::ChessPiece::WhitePawn,
                "C2" => square.current_value = pieces::ChessPiece::WhitePawn,
                "D2" => square.current_value = pieces::ChessPiece::WhitePawn,
                "E2" => square.current_value = pieces::ChessPiece::WhitePawn,
                "F2" => square.current_value = pieces::ChessPiece::WhitePawn,
                "G2" => square.current_value = pieces::ChessPiece::WhitePawn,
                "H2" => square.current_value = pieces::ChessPiece::WhitePawn,

                "A1" => square.current_value = pieces::ChessPiece::WhiteRook,
                "B1" => square.current_value = pieces::ChessPiece::WhiteKnight,
                "C1" => square.current_value = pieces::ChessPiece::WhiteBishop,
                "D1" => square.current_value = pieces::ChessPiece::WhiteQueen,
                "E1" => square.current_value = pieces::ChessPiece::WhiteKing,
                "F1" => square.current_value = pieces::ChessPiece::WhiteBishop,
                "G1" => square.current_value = pieces::ChessPiece::WhiteKnight,
                "H1" => square.current_value = pieces::ChessPiece::WhiteRook,

                _ => square.current_value = pieces::ChessPiece::Empty,
            }
        }
    }
}

pub fn draw_board(board: &Vec<Vec<BoardSquare>>) {
    println!("\n",);
    for row in 0..ROWS {
        let mut row_string = String::new();
        row_string.push_str("      ");

        for column in 0..COLUMNS {
            if &board[row][column].current_value != &pieces::ChessPiece::Empty {
                row_string.push_str(pieces::get_html_entity(&board[row][column].current_value));
            } else {
                row_string.push_str(&board[row][column].default_field);
            }
            row_string.push_str("   ");
        }
        row_string.push_str("\n");
        println!("{}", row_string);
    }
    println!("{}", "\n");
}
