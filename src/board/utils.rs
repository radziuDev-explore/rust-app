use crate::board;
use crate::pieces::ChessPiece;

pub fn is_white_piece(board: &Vec<Vec<board::BoardSquare>>, row: usize, column: usize) -> bool {
    if board[row][column].current_value == ChessPiece::WhitePawn
        || board[row][column].current_value == ChessPiece::WhiteBishop
        || board[row][column].current_value == ChessPiece::WhiteKnight
        || board[row][column].current_value == ChessPiece::WhiteRook
        || board[row][column].current_value == ChessPiece::WhiteQueen
        || board[row][column].current_value == ChessPiece::WhiteKing
    {
        return true;
    }
    return false;
}

pub fn is_black_piece(board: &Vec<Vec<board::BoardSquare>>, row: usize, column: usize) -> bool {
    if board[row][column].current_value == ChessPiece::BlackPawn
        || board[row][column].current_value == ChessPiece::BlackBishop
        || board[row][column].current_value == ChessPiece::BlackKnight
        || board[row][column].current_value == ChessPiece::BlackRook
        || board[row][column].current_value == ChessPiece::BlackQueen
        || board[row][column].current_value == ChessPiece::BlackKing
    {
        return true;
    }
    return false;
}

pub fn is_empty(board: &Vec<Vec<board::BoardSquare>>, row: usize, column: usize) -> bool {
    if board[row][column].current_value == ChessPiece::Empty {
        return true;
    }
    return false;
}

